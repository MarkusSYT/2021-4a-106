db.createCollection("discounts", {
   validator: {
      $jsonSchema: {
         bsonType: "object",
         properties: {
            image: {
               bsonType: "string"
            },
            name: {
               bsonType: "string"            
            },
            prev_price: {
               bsonType: "string"
            },
            curr_price: {
               bsonType: "string"
            },
            shop: {
               enum: ["Hofer","LIDL", "Spar", "Billa"]
            }
         }
      }
   }
});
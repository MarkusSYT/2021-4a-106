<?php
 session_start();
 $host = "localhost";
 $username = "rabatt";
 $password = "kHoJxZ8EicyEBQFv";
 $database = "discount";
 try
 {
      $connect = new PDO("mysql:host=$host; dbname=$database", $username, $password);
      $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           if(empty($_POST["username"]) || empty($_POST["password"]))
           {
                $message = '<label>Alle Felder werden benötigt</label>';
           }
           else
           {
                $query = "SELECT * FROM user WHERE username = :username";
                $statement = $connect->prepare($query);
                $statement->execute(
                     array(
                          'username' => $_POST["username"]
                     )
                );
                $count = $statement->rowCount();
                $data = $statement->fetch();
                $pass = $data['password'];

                if(password_verify($_POST['password'], $pass)){
                  if($count > 0)
                  {
                       $_SESSION["username"] = $_POST["username"];
                       header("location:page.php");
                  }
                  else
                  {
                       $message = '<label>Falsche Daten</label>'. $pass;
                  }
                }
           }
 }
 catch(PDOException $error)
 {
      $message = $error->getMessage();
 }
 ?>
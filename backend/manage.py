from flask import Flask, redirect, url_for
from pymongo import MongoClient
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/getdata/')
def getnames():
    mongoClient = MongoClient('mongodb://127.0.0.1:27017')
    db = mongoClient['discounts']
    collection = db['posts']
    products_json = list()
    print(list(collection.find({})))
    for name in collection.find():
        products_json.append({"name": name['name'], "curr_price": name['curr_price'], "prev_price": name['prev_price'], "shop": name['shop']})
    print(json.dumps(products_json))
    return json.dumps(products_json)

if __name__ == "__main__":
    app.run(debug=True)


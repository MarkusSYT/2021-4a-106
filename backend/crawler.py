import requests
from bs4 import BeautifulSoup
import links
from pymongo import MongoClient

class WebCrawler:
    def __init__(self, url: str, shop: str):
        self.url = url
        self.shop = shop

    def get_data(self):
        page = requests.get(self.url, allow_redirects=True).text
        client = MongoClient("mongodb://127.0.0.1:27017")
        db = client['discounts']
        collection = db['discounts']
        posts = db.posts
        posts.delete_many({})
        name = ""
        old = ""
        new = ""
        for i in range(12):
            name = ""
            old = ""
            new = ""
            table = BeautifulSoup(page, 'lxml').find_all('figcaption')[i]
            for row in table.find_all('h3'):
                name = str(row).replace('<h3>', '')
                name = name.replace('</h3>', '')
            for row in table.find_all('sub'):
                old = str(row)
                old = old.replace('<sub>', '')
                old = old.replace('</sub>', '')
            for row in table.find_all('b'):
                new = str(row)
                new = new.replace('<b>', '')
                new = new.replace('<b style="">', '')
                new = new.replace('</b>', '')
                new = new.replace('<sup>', '')
                new = new.replace('</sup>', '')
            post = {"name": name,
                    "prev_price": old,
                    "curr_price": new,
                    "shop": self.shop
                    }
            post_id = posts.insert_one(post).inserted_id
            
w = WebCrawler(links.getHofer(), "Hofer")
w.get_data()
from pymongo import MongoClient
import json

mongoClient = MongoClient('mongodb://127.0.0.1:27017')
db = mongoClient.get_database('discounts')
names_col = db.get_collection('discounts')
products_json = []
print(names_col.discounts.count())
for name in names_col.find():
    products_json.append({"image": name['image'], "name": name['name'], "curr_price": name['curr_price'], "prev_price": name['prev_price'], "shop": name['shop']})
print(json.dumps(products_json))
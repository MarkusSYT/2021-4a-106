import './App.css';
import React from 'react';
import axios from 'axios';

class App extends React.Component {

  state = {
    title: '',
    body: '',
    posts: []
  }
  componentDidMount = () => {
    this.getNames();
 } 
 getNames = () => {
   axios.get("http://localhost:5000/getdata/").then((response) => {
     const data = response.data;
     this.setState({ posts: data });
     console.log('Data transfered');
     console.log(data);
   }).catch(() => {
     console.log('maybe next time');
   });
 }
  displayProducts = (posts) => {
    if (!posts.length) return null;

    return posts.map((post, index) => {
      return (<div key={index}>
        <div className="card h-100 border border-secondary custom-card">
          <div className="card-body">
            <p className="card-text">{post.name}</p>
            <h5 className="card-title new_price">{post.curr_price}</h5>
            <p className="card-text old_price">{post.prev_price}</p>
            <p className="card-text shop">{post.shop}</p>
          </div>
        </div>
      </div>);
    });
  }
  render() {
    return (
      <div>
      <nav className="navbar-dark navbar navbar-expand-lg navbar-dark bg-dark" >
        <span className="mb-0 hl-light"><b>RABATTSAMMLER</b></span>
        <button className="btn btn-outline-light" type="button">Login</button>
        </nav>
        <div className="container">
          <div className="column">
            {this.displayProducts(this.state.posts)}
          </div>
        </div>
      </div>);
  }
}


export default App;